from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db import models


class Post(models.Model):
    title = models.CharField(max_length=255)
    summary = RichTextField()
    text = RichTextUploadingField()
    author = models.TextField(User, on_delete=models.PROTECT)
    date_added = models.DateField(auto_now_add=True)

    class Meta:
        ordering = ['-date_added']

